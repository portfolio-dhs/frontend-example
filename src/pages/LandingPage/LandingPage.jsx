import React, { Component } from 'react';
import axios from 'axios';

import './LandingPage.css';
import { Link } from 'react-router-dom';

class LandingPage extends Component {
  state = {
    orders: null,
  };

  componentDidMount = () => {
    axios
      .get('https://eshop-deve.herokuapp.com/api/v2/orders', {
        headers: {
          Authorization:
            'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJkM2NIVUVibVJoc1EzeXhNbzV2VnliSTFzaDZCSDJZRCIsImlhdCI6MTU4NTkzMjYzNDU0OH0.tMSht_M3ryQl5IqCirhYR1gb8j3FQ26vILT4Qpx4XrdFz-zUmqbgFYiKTaZHPpB85etRIMhxVoZf6tOrHy0fnA',
        },
      })
      .then((response) => {
        console.log(response.data.orders);
        this.setState({
          orders: response.data.orders,
        });
      });
  };

  render() {
    return (
      <main id={'landing-page'}>
        <h1 style={{ textAlign: 'center', fontSize: '2.5rem' }}>
          Available orders
        </h1>
        <p>Select an order to edit it and proceed to checkout.</p>
        <section id={'orders-container'}>
          {this.state.orders &&
            this.state.orders.map((order) => {
              return (
                <Link to={'/order/' + order.id} key={`order-${order.name}`}>
                  <button className={'order-box'}>{order.name}</button>
                </Link>
              );
            })}
        </section>
      </main>
    );
  }
}

export default LandingPage;
