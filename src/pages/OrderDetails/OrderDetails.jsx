import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import EdiText from 'react-editext';

import './OrderDetails.css';

class OrderDetails extends Component {
  modalRoot = document.getElementById('modal');
  formModalRef = React.createRef();
  payModalRef = React.createRef();

  componentDidMount = () => {
    const ORDER_ID = this.props.match.params.id;

    const URL = `https://eshop-deve.herokuapp.com/api/v2/orders/${ORDER_ID}`;
    axios
      .get(URL, {
        headers: {
          Authorization:
            'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJkM2NIVUVibVJoc1EzeXhNbzV2VnliSTFzaDZCSDJZRCIsImlhdCI6MTU4NTkzMjYzNDU0OH0.tMSht_M3ryQl5IqCirhYR1gb8j3FQ26vILT4Qpx4XrdFz-zUmqbgFYiKTaZHPpB85etRIMhxVoZf6tOrHy0fnA',
        },
      })
      .then((response) => {
        this.setState({
          orderDetails: response.data.order,
        });
      });
  };

  updateField = (fieldName, value, index) => {
    let orderDetails = this.state.orderDetails;
    let items = orderDetails.items;
    items[index][fieldName] = value;
    orderDetails.items = items;

    this.setState({
      orderDetails: orderDetails,
    });
  };

  openModal = (event) => {
    let id = event.target.id;
    let modal;
    if (id === 'open-form') {
      modal = this.formModalRef.current;
    } else if (id === 'open-alert') {
      modal = this.payModalRef.current;
      setTimeout(() => this.closeModal('pay-modal'), 5000);
    }

    if (modal) modal.style = 'display: flex';
  };

  closeModal = (modalId) => {
    let modal;

    switch (modalId) {
      case 'form-modal':
        modal = this.formModalRef.current;
        break;
      case 'pay-modal':
        modal = this.payModalRef.current;
        break;
      default:
        return;
    }

    modal.style = 'display: none';
  };

  handleModalClick = (event) => {
    if (event.target === event.currentTarget) {
      this.closeModal(event.target.id);
    }
  };

  handleChange = (event) => {
    this.setState({
      form: {
        ...this.state.form,
        [event.target.name]: event.target.value,
      },
    });
  };

  handleSubmitEvent = (event) => {
    event.preventDefault();

    if (this.state.form) {
      const { name, sku, quantity, price } = this.state.form;
      let orderDetails = this.state.orderDetails;
      let items = orderDetails.items;

      items.push({
        name,
        sku,
        quantity,
        price,
        id: btoa(Date.now().toString()),
      });

      orderDetails.items = items;
      this.setState(
        {
          orderDetails,
          form: {
            name: '',
            sku: '',
            quantity: '',
            price: '',
          },
        },
        () => {
          this.closeModal('form-modal');
        }
      );
    }
  };

  state = {
    orderDetails: null,
    form: {
      name: '',
      sku: '',
      quantity: '',
      price: '',
    },
  };

  render() {
    return (
      <main>
        <br />
        <section id={'table'}>
          <h1 style={{ textAlign: 'center', fontSize: '2.5rem' }}>
            Order Checkout
          </h1>
          <p>
            Click on the dotted lines to change the fields and click on "Pay!"
            button for procceed to paying.
          </p>
          <hr />
          {this.state.orderDetails &&
            this.state.orderDetails.items.map((item, i) => {
              let name = this.state.orderDetails.items[i].name || 'NO NAME';
              let sku = this.state.orderDetails.items[i].sku || 'NO SKU';
              let quantity = this.state.orderDetails.items[i].quantity || 1;
              let price = this.state.orderDetails.items[i].price || 1;

              return (
                <React.Fragment key={'item-' + item.id}>
                  <article className={'table-cell'}>
                    <header className={'table-names'}>
                      <EdiText
                        showButtonsOnHover
                        editOnViewClick
                        cancelOnUnfocus
                        submitOnEnter
                        cancelOnEscape
                        value={name}
                        type={'text'}
                        onSave={(value) => {
                          this.updateField('name', value, i);
                        }}
                      />
                      <br />
                      <label htmlFor={'sku-input'}>
                        SKU:
                        <EdiText
                          showButtonsOnHover
                          editOnViewClick
                          cancelOnUnfocus
                          submitOnEnter
                          cancelOnEscape
                          value={sku}
                          type={'text'}
                          inputProps={{
                            id: 'sku-input',
                          }}
                          onSave={(value) => {
                            this.updateField('sku', value, i);
                          }}
                        />
                      </label>
                    </header>
                    <label htmlFor={'quantity-input'}>
                      Quantity
                      <EdiText
                        showButtonsOnHover
                        editOnViewClick
                        cancelOnUnfocus
                        submitOnEnter
                        cancelOnEscape
                        mainContainerClassName={'table-quantity'}
                        value={quantity}
                        type={'text'}
                        inputProps={{
                          id: 'quantity-input',
                        }}
                        onSave={(value) => {
                          this.updateField('quantity', value, i);
                        }}
                      />
                    </label>
                    <label htmlFor={'quantity-input'}>
                      Price:
                      <EdiText
                        showButtonsOnHover
                        editOnViewClick
                        cancelOnUnfocus
                        submitOnEnter
                        cancelOnEscape
                        mainContainerClassName={'table-price'}
                        value={price}
                        type={'text'}
                        inputProps={{
                          id: 'price-input',
                        }}
                        onSave={(value) => {
                          this.updateField('price', value, i);
                        }}
                      />
                    </label>
                  </article>
                  <hr />
                </React.Fragment>
              );
            })}
          <button
            type={'button'}
            id={'open-form'}
            className={'btn'}
            onClick={this.openModal}
          >
            <strong>+</strong>
          </button>
          <br />
          <br />
          <button
            type={'button'}
            id={'open-alert'}
            className={'btn'}
            style={{ backgroundColor: 'green', fontWeight: 'bolder' }}
            onClick={this.openModal}
          >
            Pay!
          </button>
        </section>
        {ReactDOM.createPortal(
          <React.Fragment>
            <section
              id={'form-modal'}
              onClick={this.handleModalClick}
              ref={this.formModalRef}
            >
              <form onSubmit={this.handleSubmitEvent}>
                <button
                  id={'ad'}
                  className={'close btn'}
                  type={'button'}
                  onClick={() => this.closeModal('form-modal')}
                >
                  ×
                </button>
                <h3>New record form</h3>
                <label htmlFor="name">
                  Name:
                  <input
                    type="text"
                    id={'name'}
                    name={'name'}
                    className={'input'}
                    onChange={this.handleChange}
                    value={this.state.form.name}
                    required
                  />
                </label>
                <label htmlFor="sku">
                  SKU:
                  <input
                    type="text"
                    id={'sku'}
                    name={'sku'}
                    className={'input'}
                    onChange={this.handleChange}
                    value={this.state.form.sku}
                    required
                  />
                </label>
                <label htmlFor="quantity">
                  Quantity:
                  <input
                    type="number"
                    id={'quantity'}
                    name={'quantity'}
                    className={'input'}
                    onChange={this.handleChange}
                    value={this.state.form.quantity}
                    required
                  />
                </label>
                <label htmlFor="price">
                  Price:
                  <input
                    type="number"
                    id={'price'}
                    name={'price'}
                    className={'input'}
                    onChange={this.handleChange}
                    value={this.state.form.price}
                    required
                  />
                </label>
                <br />
                <button type={'submit'} className={'btn'}>
                  Submit
                </button>
              </form>
            </section>
            <section
              id={'pay-modal'}
              ref={this.payModalRef}
              onClick={this.handleModalClick}
            >
              <article
                style={{
                  padding: '30% 2rem',
                  backgroundColor: 'green',
                  color: 'white',
                  fontSize: '3rem',
                }}
              >
                <div>
                  <span>The payment has been processed</span>
                </div>
              </article>
            </section>
          </React.Fragment>,
          this.modalRoot
        )}
      </main>
    );
  }
}

export default OrderDetails;
