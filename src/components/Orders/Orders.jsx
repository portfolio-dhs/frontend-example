import React from 'react';
import PropTypes from 'prop-types';

const Orders = (props) => {
  return (
    <article>
      Name: {props.name}
      <br />
      SKU: {props.sku}
      <br />
      Quantity: {props.quantity}
      <br />
      Price: {props.price}
    </article>
  );
};

Orders.propTypes = {
  name: PropTypes.string.isRequired,
  sku: PropTypes.string.isRequired,
  quantity: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
};

export default Orders;
