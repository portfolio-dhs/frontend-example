import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import LandingPage from './pages/LandingPage/LandingPage';
import OrderDetails from './pages/OrderDetails/OrderDetails';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={'/'} component={LandingPage} />
        <Route path={'/order/:id'} component={OrderDetails} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
